<?php
require_once __DIR__ . '/vendor/autoload.php';

use app\models\Hoge;

$routes = [
	'GET /' => function() {
		echo '<script src="/public/build/main.js" async defer></script>';
		echo '<link rel="stylesheet" href="/public/build/style.css">';
		(new Hoge)->greet();
		echo '<pre><code>';
		echo h(dumped_text($_SERVER ?? null));
		echo '</code></pre>';
	},
	'GET /hoge' => function() {
		?>
		<form action="/fuga" method="post">
		<button type="submit">ok</button>
		</form>
		<?php
	},
	'POST /fuga' => fn() => 'fuga',
	'GET /books/:id' => fn($p) => 'book / <pre>' . print_r($p, true),
	'GET /users/:id' => fn($p, $n) => preg_match('/^\\d+$/mi', $p['id']) ? 'user / <pre>' . print_r($p, true) : $n(),
];


$res = handle_request($_SERVER['REQUEST_METHOD'] ?? '', $_SERVER['REQUEST_URI'] ?? '', $routes);
if (isset($res['error'])) throw $res['error'];
if (isset($res['code'])) http_response_code($res['code']);
exit($res['ok']);
