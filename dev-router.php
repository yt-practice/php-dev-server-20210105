<?php

if (
	\preg_match('/\\.[a-z0-9]{2,4}$/mi', $_SERVER['REQUEST_URI'] ?? '')
	and \file_exists(__DIR__ . $_SERVER['REQUEST_URI'])
	and !\preg_match('/\\.php$/mi', $_SERVER['REQUEST_URI'])
	and !\preg_match('|/\\.|i', $_SERVER['REQUEST_URI'])
) return false;

require __DIR__ . '/index.php';
