<?php

function h(string $in = null): string {
	return htmlspecialchars($in ?? '', ENT_QUOTES, 'UTF-8');
}

function dumped_text($o): string {
	ob_start();
	var_dump($o);
	return ob_get_clean();
}

function handle_request(string $request_method, string $request_uri, array $routes): array {
	$reqpath = explode('/', rtrim($request_uri, '/') ?: '/');
	$nextex = new class('next', 1) extends Exception {};
	$next = function() use ($nextex) {
		throw $nextex;
	};
	$run = function($body, $params) use ($next, $nextex) {
		ob_start();
		try {
			$returned = '' . ($body($params, $next) ?? '');
			$mode = 'exit';
		} catch (Exception $th) {
			$mode = $nextex === $th ? 'continue' : 'throw';
		}
		$dumped = ob_get_clean();
		if ('exit' === $mode) return ['ok' => $dumped . $returned];
		if ('continue' === $mode) return 'continue';
		if ('throw' === $mode) return ['error' => $th];
	};
	foreach ($routes as $p => $body) {
		if ('404' === $p) continue;
		$path = explode(' ', $p);
		$methods = explode('|', array_shift($path));
		if (!in_array($request_method, $methods, true)) continue;
		$chpath = explode('/', join(' ', $path));
		$bottom = count($chpath) - 1;
		if (count($chpath) !== count($reqpath) and !str_starts_with($chpath[$bottom], '...')) continue;
		$params = [];
		foreach ($chpath as $i => $p) {
			if (str_starts_with($p, ':')) {
				$params[substr($p, 1)] = $reqpath[$i];
			} elseif (str_starts_with($p, '...') and $i == $bottom) {
				$params[substr($p, 3)] = join('/', array_slice($reqpath, $i));
				break;
			} elseif ($p !== $reqpath[$i]) {
				continue 2;
			}
		}
		$res = $run($body, $params);
		if ('continue' === $res) continue;
		return $res;
	}
	$res = ($body = $routes['404'] ?? null) ? $run($body, []) : [];
	if ('continue' === $res) return ['error' => new Error('can\'t continue.')];
	if (isset($res['error'])) return $res;
	return ['ok' => $res['ok'] ?? 'not found.', 'code' => 404];
}
